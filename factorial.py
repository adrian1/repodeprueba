#!/usr/bin/python3

lista = ["a", "b", "c", 10]

diccionario = {
    "hola": "te saludo",
    "adios": "me despido"
}


def factorial(n):
    fact = 1
    while n > 1:
        fact = fact * n
        n = n - 1
    return fact


if __name__ == "__main__":
    for i in range(1, 11):
        print(factorial(i))
