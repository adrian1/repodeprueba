#!/usr/bin/python3
# Este es un programa que maneja el flujo de argumento
from sys import argv
from factorial import factorial

FACTORIAL = "FACTORIAL"
SUMA = "SUMA"

def suma(int1, int2):
    return int1 + int2

if __name__ == "__main__":
    if argv[1].upper() == FACTORIAL:
        # EHecutar el factorial del segundo argumento, pasandolo a entero
        fact = factorial(int(argv[2]))
        print(fact)
    elif argv[1].upper() == SUMA:
        try:
            resultado_suma = suma(int(argv[2]), int(argv[3]))
            print(resultado_suma)
        except ValueError:
            print("Debes de poner los argumentos correctamente, ejemplo: ./argumentos.py suma 5 5")

